<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
Đây là nơi bạn có thể đăng ký các tuyến API cho ứng dụng của mình. Các tuyến này được tải bởi RouteServiceProvider trong một nhóm được gán nhóm trung gian "api". Thích xây dựng API của bạn!
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
